package com.example.merofortisdpruevasql;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText   id ,txtregistro,txtrectora,txtrespecialidad,txtsecion,txtformacion;
    private Button guardar, leer, leerT, actualizar, Eliminar, eliminarT;
    private TextView Datos;
    private DataBase dataBase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtregistro = (EditText)findViewById(R.id.txtregistro);
        txtrectora = (EditText)findViewById(R.id.txtrectora);
        txtrespecialidad = (EditText)findViewById(R.id.txtrespecialidad);
        txtsecion = (EditText)findViewById(R.id.txtsecion);
        txtformacion = (EditText)findViewById(R.id.txtformacion);
        id = (EditText)findViewById(R.id.Id);
        guardar = (Button)findViewById(R.id.Guardar);
        leerT = (Button)findViewById(R.id.LeerT);
        actualizar = (Button)findViewById(R.id.Actualizar);
        eliminarT = (Button)findViewById(R.id.EliminarT);
        //Eliminar = (Button)findViewById(R.id.Eliminar);
        leer = (Button)findViewById(R.id.Leer);

        guardar.setOnClickListener(this);
        leerT.setOnClickListener(this);
        leer.setOnClickListener(this);
        //Eliminar.setOnClickListener(this);
        actualizar.setOnClickListener(this);
        eliminarT.setOnClickListener(this);
        Datos = (TextView)findViewById(R.id.Datos);
        dataBase = new DataBase(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.Guardar:

                if (txtregistro.getText().toString().isEmpty()){

                }else if(txtregistro.getText().toString().isEmpty()){

                }else if(txtrectora.getText().toString().isEmpty()){

                }else if (txtrespecialidad.getText().toString().isEmpty()){

                }else if(txtsecion.getText().toString().isEmpty()){

                }else if(txtformacion.getText().toString().isEmpty()){

                }else {
                    dataBase.Insertar(txtregistro.getText().toString(), txtrectora.getText().toString(),
                            txtrespecialidad.getText().toString(), txtsecion.getText().toString(), txtformacion.getText().toString());
                    Toast.makeText(this, "GUARDADO", Toast.LENGTH_SHORT).show();
                    id.setText("");
                    txtregistro.setText("");
                    txtrectora.setText("");
                    txtrespecialidad.setText("");
                    txtsecion.setText("");
                    txtformacion.setText("");
                    Datos.setText("");
                }

                break;

            case R.id.LeerT:
                Datos.setText(dataBase.LeerTodos());
                id.setText("");
                txtregistro.setText("");
                txtrectora.setText("");
                txtrespecialidad.setText("");
                txtsecion.setText("");
                txtformacion.setText("");
                Toast.makeText(this, "LISTADOS", Toast.LENGTH_SHORT).show();
                break;

            case R.id.Leer:
                Datos.setText(dataBase.Leer(id.getText().toString()));
                id.setText("");
                txtregistro.setText("");
                txtrespecialidad.setText("");
                txtsecion.setText("");
                txtformacion.setText("");
                Toast.makeText(this, "LISTADO", Toast.LENGTH_SHORT).show();

                break;

            /*case R.id.Eliminar:
                dataBase.Eliminar(id.getText().toString());
                id.setText("");
                Siglas.setText("");
                Nombres.setText("");
                Carreras.setText("");
                Categoria.setText("");
                Ciudad.setText("");
                Datos.setText("");
                Toast.makeText(this, "ELIMINADO", Toast.LENGTH_SHORT).show();
                break; */

            case R.id.Actualizar://MODIFICAR
                if (txtregistro.getText().toString().isEmpty()){

                }else if(txtregistro.getText().toString().isEmpty()){

                }else if(txtrectora.getText().toString().isEmpty()){

                }else if (txtrespecialidad.getText().toString().isEmpty()){

                }else if(txtsecion.getText().toString().isEmpty()){

                }else if(txtformacion.getText().toString().isEmpty()){

                }else if(id.getText().toString().isEmpty()){

                }else {
                    dataBase.Actualizar(id.getText().toString(), txtregistro.getText().toString(), txtrectora.getText().toString(),
                            txtrespecialidad.getText().toString(), txtsecion.getText().toString(), txtformacion.getText().toString());
                    Toast.makeText(this, "ACTUALIZADO", Toast.LENGTH_SHORT).show();
                    id.setText("");
                    txtregistro.setText("");
                    txtrectora.setText("");
                    txtrespecialidad.setText("");
                    txtsecion.setText("");
                    txtformacion.setText("");
                    Datos.setText("");
                }
                break;

            case R.id.EliminarT:
                dataBase.EliminarTodo();
                Datos.setText("");
                Toast.makeText(this, "ELIMINADOS", Toast.LENGTH_SHORT).show();
                break;
        }
    }

}
